# minitest

Sample repository for testing `.gitlab-ci.yml` configurations for unit tests.

This has sample pytest specs from this [gist](https://gist.github.com/hectorcanto/40a7ecbd9e02b0550840f850165bc162).
