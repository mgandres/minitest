import pytest

def raise_exception():
    raise KeyError("This function raises an exception")

def test_raise_exception():
    with pytest.raises(KeyError):
        raise KeyError("Is expected")

    with pytest.raises(KeyError):
        raise_exception()

    with pytest.raises(KeyError) as raised_exception:
        raise_exception()
        assert raised_exception.msg == "This function raises an exception."

@pytest.mark.xfail()
def test_raise_unexpected_exception():
    raise AttributeError

@pytest.mark.xfail(raises=KeyError)
def test_expected_other_exception():
    with pytest.raises(AttributeError):
        raise_exception()
