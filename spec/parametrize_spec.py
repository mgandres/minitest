import pytest

def make_sum(a, b):
    return sum([a, b])

@pytest.mark.parametrize("first_summand, second_summand, expected", [
    (1, 1, 2),
    (1, 2, 3),
    (1, -1, 0),
    (12, 12, 24)
])
def test_parametrize(first_summand, second_summand, expected):
    assert make_sum(first_summand, second_summand) == expected

@pytest.mark.parametrize("first_summand, second_summand, exception", [
    (1, "a", TypeError),
    (1, [2], TypeError),
])
def test_parametrize_exception(first_summand, second_summand, exception):
    with pytest.raises(exception):
        make_sum(first_summand, second_summand)
